;;; This file is part of mc.alpha.lyr. It is subject to the license terms in the
;;; the LICENSE file found in the top-level directory of this distribution and at
;;; https://gitlab.com/modelingcooperation/mc.alpha.lyr/-/blob/main/LICENSE.txt.
;;; No part of mc.alpha.lyr, including this file, may be copied, modified,
;;; propagated, or distributed except according to the terms contained in the
;;; LICENSE file.

(ns com.modelingcooperation.mc.alpha.lyr
  (:require [clojure.set :as set]
            [clojure.walk :as walk]
            [reagent.core :as r]
            [reagent.dom :as rd]
            [vega :as vega]
            [vega-embed :as vega-embed]))

(defn- apply-log-level
  [{:as opts :keys [log-level]}]
  (if (or (keyword? log-level) (string? log-level))
    (-> opts
        (dissoc :log-level)
        (assoc :logLevel
               (case (keyword log-level)
                 :debug vega/Debug
                 :info vega/Info
                 :warn vega/Warn)))
    opts))

(defn ^:no-doc embed-vega
  ([elem doc] (embed-vega elem doc {}))
  ([elem doc opts]
   (when doc
     (let [doc (clj->js doc)
           opts (->> opts
                     (merge {:renderer :canvas
                             :mode "vega-lite"})
                     (apply-log-level))
           opts (merge {:renderer :canvas}
                       opts)]
       (-> ((.-default vega-embed) elem doc (clj->js opts))
           (.catch (fn [err]
                     (js/console.log err))))))))

(defn ^:no-doc update-vega
  ([elem old-doc new-doc old-opts new-opts]
   ;; Only rerender from scratch if the viz specification has actually
   ;; changed, or if always rerender is specified.
   (when (or (:always-rerender new-opts)
             (not= (dissoc old-doc :data) (dissoc new-doc :data))
             (not= old-opts new-opts))
     (embed-vega elem new-doc new-opts))))

(defn vega
  "Reagent component that renders Vega"
  ([doc] (vega doc {}))
  ([doc opts]
   (let [opts (merge {:mode "vega"} opts)]
     (r/create-class
      {:display-name "vega"
       :component-did-mount (fn [this]
                              (embed-vega (rd/dom-node this) doc opts))
       ;; :component-did-update only receives the previous state as arguments.
       ;; This means we need to manually get the current state from the
       ;; component.
       :component-did-update (fn [this _]
                               (let [[_ new-doc new-opts] (r/argv this)]
                                 (embed-vega (rd/dom-node this)
                                             new-doc
                                             new-opts)))
       :reagent-render (fn [_]
                         [:div.viz])}))))

(defn vega-lite
  "Reagent component that renders Vega-Lite."
  ([doc] (vega-lite doc {}))
  ([doc opts]
   ;; Which way should the merge go?
   (vega doc (merge opts {:mode "vega-lite"}))))

(def ^:private live-viewers-state
  (r/atom {:vega vega
           :vega-lite vega-lite}))

(defn register-live-view
  [k component]
  (swap! live-viewers-state assoc k component))

(defn register-live-views
  [& {:as live-views}]
  (swap! live-viewers-state merge live-views))

(def default-data-table-opts
  {:per-page 50
   :tr-style {}
   :td-style {:padding-right 10}
   :th-style {:padding-right 10
              :cursor :pointer}})

(defn data-table
  ([data] (data-table data {}))
  ([data {:keys [page sort-key sort-order]}]
   (let [state (r/atom {:page (or page 0)
                        :sort-key sort-key
                        :sort-order (or sort-order :ascending)})
         header (->> data
                     (take 10)
                     (map (comp set keys))
                     (reduce set/union))]
     (fn [data opts]
       (let [{:keys [page sort-key sort-order]} @state
             {:keys [per-page tr-style td-style th-style]}
             (merge-with (fn [opt1 opt2]
                           (if (and (map? opt1) (map? opt2))
                             (merge opt1 opt2)
                             opt2))
                         default-data-table-opts
                         opts)
             scoped-data (cond->> data
                           sort-key (sort-by sort-key)
                           (= :descending sort-order) (reverse)
                           per-page (drop (* per-page page))
                           per-page (take per-page))
             last-page (quot (count data) per-page)]
         [:div
          (when (> (count data) per-page)
            [:p
             {:style {:margin-bottom 10}}
             [:span
              {:style {:padding-right 20}}
              "Current page: " (inc page)]
             (when (> page 0)
               [:a
                {:on-click (fn [& _] (swap! state update :page dec))
                 :style {:padding-right 10
                         :cursor :pointer}}
                "prev"])
             (when (< page last-page)
               [:a
                {:on-click (fn [& _] (swap! state update :page inc))
                 :style {:padding-right 10
                         :cursor :pointer}}
                "next"])])
          [:table
           ;; header row
           [:tr
            {:style tr-style}
            (for [k header]
              ^{:key k}
              [:th {:style th-style
                    :on-click (fn [& _]
                                (swap! state
                                       merge
                                       {:sort-key k
                                        :sort-order (if (and (= k sort-key)
                                                             (= sort-order
                                                                :ascending))
                                                      :descending
                                                      :ascending)}))}
               (name k)
               (when (= sort-key k)
                 (case sort-order
                   :ascending "⌃"
                   :descending "⌄"))])]
           (for [row scoped-data]
             ^{:key (hash row)}
             [:tr
              {:style tr-style}
              (for [k header]
                ^{:key k}
                [:td {:style td-style} (get row k)])])]])))))

(register-live-views
 :vega vega
 :vega-lite vega-lite
 :data-table data-table)

(defn ^:no-doc live-view
  [doc]
  ;; Prewalk spec, rendering special hiccup tags like :vega and :vega-lite, and
  ;; potentially other composites, rendering using the components above. Leave
  ;; regular hiccup unchanged).
  (let [live-viewers @live-viewers-state
        live-viewer-keys (set (keys live-viewers))]
    (clojure.walk/prewalk
     (fn [x] (if (and (coll? x) (live-viewer-keys (first x)))
               (into
                [(get live-viewers (first x))]
                (rest x))
               x))
     doc)))

(def ^:no-doc view-spec live-view)
